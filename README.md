## Vanilla ArgoCD

Part of an example umbrella chart that deployes a basic services stack to a k8s cluster.

Prerequisites:
- A k8s cluster.
- `kubectl` and a privillaged kubeconfig file.
- `Helm` v3 binary.
- Service type `LoadBalancer` (MetalLB, Keepalived Operator, or other cloud native services).

How to:
- Run `helm install vanilla-argocd ./` (or deploy the packaged version .tgz file via the umrella chart).
- In order to get the `ExternalIp` of the argocd-server (the UI) use `kubectl get svc -n argocd | grep LoadBalancer | awk '{print $4}'`
- In order to retrieve the auto-generated initial password run `kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d`.
- Login to the server and set the password via the UI or the CLI.
- Delete the `argocd-initial-admin-secret` in the argocd namespace once you changed the password: ` kubectl delete secret argocd-initial-admin-secret -n argocd`

PLEASE NOTICE:
- This deployment doest consider ssl termination, the server will serve http requests only.
- This is a none HA deployment.
- There is no ingress deployed as part of the chart's menifests! cant access the argo-cd server UI via FQDN.
